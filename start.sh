#!/bin/bash

set -e

function handle_signal {
  PID=$!
  echo "received signal. PID is ${PID}"
  kill -s SIGHUP $PID
}

trap "handle_signal" SIGINT SIGTERM SIGHUP

echo "starting radarr"

mono /opt/radarr/Radarr.exe --no-browser -data=/data/config/radarr & wait

echo "stopping radarr"
