FROM lsiobase/mono:bionic

# mono 3.10 currently doesn't install in debian jessie due to libpeg8 being removed.

RUN  apt-get update -q \
  && apt-get install -qy mediainfo jq curl \
  && apt-get clean \

# environment settings
ARG DEBIAN_FRONTEND="noninteractive"
ENV XDG_CONFIG_HOME="/config/xdg"

RUN \
 RADARR_RELEASE=$(curl -sX GET "https://api.github.com/repos/Radarr/Radarr/releases" | jq -r '.[0] | .tag_name') && \
 radarr_url=$(curl -s https://api.github.com/repos/Radarr/Radarr/releases/tags/"${RADARR_RELEASE}" |jq -r '.assets[].browser_download_url' |grep linux) && \
 mkdir -p /opt/radarr && \
 curl -o /tmp/radar.tar.gz -L "${radarr_url}" && \
 tar ixzf /tmp/radar.tar.gz -C /opt/radarr --strip-components=1 && \
 rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

EXPOSE 7878

ADD ./start.sh /radarr/start.sh

VOLUME /volumes/config
VOLUME /volumes/completed
VOLUME /volumes/media

ENTRYPOINT ["/radarr/start.sh"]
